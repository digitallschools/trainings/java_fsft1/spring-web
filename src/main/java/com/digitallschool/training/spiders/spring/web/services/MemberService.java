/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web.services;

import com.digitallschool.training.spiders.spring.web.model.Member;
import com.digitallschool.training.spiders.spring.web.repository.MemberRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Service
public class MemberService {

    @Autowired
    MemberRepository repository;
    
    public Member getMemberById(int memberId){
        return repository.getMember(memberId);
    }

    public boolean deleteMember(int memberId){
        return repository.deleteMember(memberId);
    }
    
    public boolean updateMember(Member m){
        return repository.updateMember(m);
    }
    
    public boolean addMember(Member m) {
        return repository.addMember(m);
    }

    public List<Member> getMembers() {
        return repository.getAllMembers();
    }
}
