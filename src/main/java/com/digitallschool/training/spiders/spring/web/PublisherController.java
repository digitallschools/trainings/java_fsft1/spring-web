/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web;

import com.digitallschool.training.spiders.spring.web.model.Publisher;
import com.digitallschool.training.spiders.spring.web.services.PublisherService;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Controller
@RequestMapping("/publishers")
//@ResponseBody
public class PublisherController {

    //@Autowired
    PublisherService publisherService;

    //@Autowired
    public PublisherController(PublisherService service) {
        this.publisherService = service;
    }

    @RequestMapping("/all")
    public String viewPublishers(Model model) {
        model.addAttribute("publishersData", publisherService.getPublishers());

        return "publishers/viewPublishers";
    }

    @RequestMapping("/publishers/hello")
    @ResponseBody
    public String sayHello() {
        return LocalDateTime.now() + " { Saying Hello to PUBLISHERS }";
    }

    @RequestMapping(path = "/allDirect", produces = "application/json",
            method = RequestMethod.GET)
    @ResponseBody
    public List<Publisher> viewPublishers() {
        return publisherService.getPublishers();
    }
    
    /*@ResponseBody
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public String handleHttpMediaTypeNotAcceptableException() {
        return "acceptable MIME type:" + MediaType.APPLICATION_JSON_VALUE;
    }*/
}
