/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Controller
public class WelcomeController {

    @RequestMapping("/hi")
    public String sayHello(Model model) {
        model.addAttribute("message", "Hello, Welcome to Spring Web World !");
        return "hello";
    }
}
