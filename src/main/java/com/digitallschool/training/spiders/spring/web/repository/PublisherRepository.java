/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web.repository;

import com.digitallschool.training.spiders.spring.web.model.Publisher;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Repository
public class PublisherRepository {

    @Autowired
    DataSource dataSource;

    public List<Publisher> allPublishers() {
        List<Publisher> all = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM publishers")) {

            while (rs.next()) {
                all.add(
                        new Publisher(rs.getInt(1), rs.getString(2),
                                rs.getString(5), rs.getString(4), rs.getString(3))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return all;
    }
}
