/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web.services;

import com.digitallschool.training.spiders.spring.web.model.Author;
import com.digitallschool.training.spiders.spring.web.repository.AuthorRepository;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Service
public class AuthorService {

    @Autowired
    AuthorRepository ar;

    public boolean addAuthor(Author author) {
        if (Objects.isNull(author) || Objects.isNull(author.getName().trim())
                || author.getName().trim().length() == 0) {
            return false;
        } else {
            return ar.addAuthor(author);
        }
    }

    public List<Author> getAllAuthors() {
        return ar.getAuthors();
    }

    public Author getAuthorById(int authorId) {
        return ar.getAuthor(authorId);
    }
    
    public boolean updateAuthor(Author author){
        return ar.updateAuthor(author);
    }
}
