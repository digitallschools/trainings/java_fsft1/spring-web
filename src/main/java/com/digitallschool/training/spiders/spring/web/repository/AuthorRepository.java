/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web.repository;

import com.digitallschool.training.spiders.spring.web.model.Author;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Repository
public class AuthorRepository {

    @Autowired
    DataSource ds;

    public boolean addAuthor(Author author) {
        try (Connection con = ds.getConnection();
                PreparedStatement pst = con.prepareStatement("INSERT INTO authors VALUES(0, ?, ?, ?, ?)")) {

            pst.setString(1, author.getName());
            pst.setString(2, author.getEmail());
            pst.setString(3, author.getProfile());
            pst.setString(4, author.getBlog());

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<Author> getAuthors() {
        List<Author> authors = null;

        try (Connection con = ds.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM authors")) {
            authors = new ArrayList<>();

            while (rs.next()) {
                authors.add(
                        new Author(rs.getInt(1), rs.getString(2),
                                rs.getString(3), rs.getString(4),
                                rs.getString(5)
                        )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return authors;
    }

    public Author getAuthor(int authorId) {
        try (Connection con = ds.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM authors WHERE author_id=" + authorId)) {
            if (rs.next()) {
                return new Author(rs.getInt(1), rs.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean updateAuthor(Author author){
        try (Connection con = ds.getConnection();
                PreparedStatement pst = con.prepareStatement("UPDATE authors SET name=?, email=?, profile=?, blog=? "
                        + "WHERE author_id=?")) {

            pst.setString(1, author.getName());
            pst.setString(2, author.getEmail());
            pst.setString(3, author.getProfile());
            pst.setString(4, author.getBlog());
            pst.setInt(5, author.getAuthorId());

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
