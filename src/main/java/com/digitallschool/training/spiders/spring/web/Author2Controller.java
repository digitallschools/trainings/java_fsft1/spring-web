/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web;

import com.digitallschool.training.spiders.spring.web.model.Author;
import com.digitallschool.training.spiders.spring.web.services.AuthorService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Controller
@RequestMapping("/authors2")
@SessionAttributes("author")
public class Author2Controller {

    @Autowired
    AuthorService service;

    @GetMapping
    public String viewAuthors(Model m) {
        setupAuthors(m);
        return "authors/home";
    }

    @PostMapping
    public String addAuthor(Model m, @Valid @ModelAttribute("author")Author author,
            BindingResult bindingResult, SessionStatus status) {
        if (bindingResult.hasErrors()) {
            setupAuthors(m);
            return "/authors/home";
        }

        service.addAuthor(author);
        status.setComplete();
        return "redirect:/authors2";
    }

    @GetMapping("/updateForm")
    public String updateAuthorForm(@RequestParam int authorId, Model m) {
        m.addAttribute("currentAuthor", service.getAuthorById(authorId));
        return "authors/updateAuthor";
    }

    @PostMapping("/update")
    public String updateAuthor(Author currentAuthor) {
        service.updateAuthor(currentAuthor);
        return "redirect:/authors/home";
    }

    public void setupAuthors(Model model) {
        model.addAttribute("allAuthors", service.getAllAuthors());
    }
    
    @ModelAttribute("author")
    public Author setupAuthor(){
        return new Author();
    }
}
