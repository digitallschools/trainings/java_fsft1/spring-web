/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web.repository;

import com.digitallschool.training.spiders.spring.web.model.Member;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Repository
public class MemberRepository {

    @Autowired
    DataSource dataSource;

    public Member getMember(int memberId) {
        try (Connection con = dataSource.getConnection();
                PreparedStatement pst = con.prepareStatement(
                        "SELECT * FROM members WHERE member_id=?");) {

            pst.setInt(1, memberId);

            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return new Member(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getDate(5).toLocalDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteMember(int memberId) {
        try (Connection con = dataSource.getConnection();
                PreparedStatement pst = con.prepareStatement(
                        "DELETE FROM members WHERE member_id=?");) {

            pst.setInt(1, memberId);

            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateMember(Member m) {
        try (Connection con = dataSource.getConnection();
                PreparedStatement pst = con.prepareStatement(
                        "UPDATE members SET name=?, email=?, mobile=? WHERE member_id=?");) {

            pst.setString(1, m.getName());
            pst.setString(2, m.getEmail());
            pst.setString(3, m.getMobile());
            pst.setInt(4, m.getMemberId());

            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean addMember(Member m) {
        try (Connection con = dataSource.getConnection();
                PreparedStatement pst = con.prepareStatement("INSERT INTO members VALUES(0, ?, ?, ?, ?)");) {

            pst.setString(1, m.getName());
            pst.setString(2, m.getEmail());
            pst.setString(3, m.getMobile());
            pst.setDate(4, Date.valueOf(m.getRegisteredOn().toString()));

            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<Member> getAllMembers() {
        try (Connection con = dataSource.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM members")) {

            List<Member> temp = new ArrayList<>();

            while (rs.next()) {
                temp.add(
                        new Member(rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getDate(5).toLocalDate())
                );
            }

            return temp;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
