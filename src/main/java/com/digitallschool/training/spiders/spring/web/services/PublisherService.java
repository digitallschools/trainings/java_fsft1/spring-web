/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web.services;

import com.digitallschool.training.spiders.spring.web.model.Publisher;
import com.digitallschool.training.spiders.spring.web.repository.PublisherRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Service
public class PublisherService {
    @Autowired
    PublisherRepository publisherRepository;
    
    public List<Publisher> getPublishers(){
        return publisherRepository.allPublishers();
    }
}
