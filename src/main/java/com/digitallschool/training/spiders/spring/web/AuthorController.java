/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web;

import com.digitallschool.training.spiders.spring.web.model.Author;
import com.digitallschool.training.spiders.spring.web.services.AuthorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Controller
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    AuthorService service;

    @GetMapping(value = "/find/{authorId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Author findAuthor(@PathVariable("authorId") int id) {
        return service.getAuthorById(id);
    }

    @GetMapping(produces = {MediaType.TEXT_HTML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    //public String viewAuthors(@RequestHeader("Accept") List<String> acceptHeader) {
    public String viewAuthors(
            @RequestHeader(name = "Accept", required = false) String acceptHeader) {
        /*for(String value: acceptHeader){
            if(value.contains("application/json")){
                return service.getAllAuthors().toString();
            }
        }*/
        if (Objects.nonNull(acceptHeader) && acceptHeader.contains("application/json")) {
            //return service.getAllAuthors().toString();
            try {
                return new ObjectMapper().writeValueAsString(service.getAllAuthors());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "<html><body>"
                + "<form method='post'>"
                + "<input type='submit' value='Add Author'/>"
                + "</form>"
                + service.getAllAuthors()
                + "</body></html>";
    }

    @PostMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addAuthor(Author author) {
        service.addAuthor(author);
    }

    @GetMapping("/addAuthor")
    public String addAuthorForm() {
        return "/authors/addAuthor";
    }

    @GetMapping("/add/{authorId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addAuthor(@MatrixVariable String name,
            @MatrixVariable String email,
            @MatrixVariable String profile,
            @MatrixVariable String blog) {
        Author a = new Author(0, name, email, profile, blog);

        service.addAuthor(a);
    }

    @GetMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addAuthorWithRequestParams(
            @RequestParam("name") String name, @RequestParam String email,
            @RequestParam String profile, @RequestParam String blog) {
        Author a = new Author(0, name, email, profile, blog);

        service.addAuthor(a);
    }

    @ModelAttribute
    public void prepareAuthor() {

    }
}
