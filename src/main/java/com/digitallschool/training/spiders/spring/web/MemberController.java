/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring.web;

import com.digitallschool.training.spiders.spring.web.model.Member;
import com.digitallschool.training.spiders.spring.web.services.MemberService;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@RestController
@RequestMapping("/members")
public class MemberController {

    @Autowired
    MemberService service;

    @GetMapping(path = "/{id}/{property}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getMemberByIdAndProperty(
            @PathVariable("id") int memberId, @PathVariable(name = "property") String property) {

        Member member = service.getMemberById(memberId);

        if (property.equals("name")) {
            return ResponseEntity.ok().body("{\"name\": \"" + member.getName() + "\"}");
        } else if (property.equals("email")) {
            return ResponseEntity.ok().body("{\"email\": \"" + member.getEmail() + "\"}");
        }

        return ResponseEntity.ok().body(member);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Member getMemberById(@PathVariable("id") int memberId) {
        return service.getMemberById(memberId);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMember(@RequestParam("memberId") int memberId) {
        service.deleteMember(memberId);
        return ResponseEntity.ok("Successfully Deleted");
    }

    @PutMapping
    public ResponseEntity<String> updateMember(@Valid Member member, BindingResult result) {
        if (!result.hasErrors() && service.updateMember(member)) {
            return ResponseEntity.ok("Successfully Updated");
        } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Incomplete or Invalid Data: " + result.toString());
        }
    }

    @PostMapping
    public ResponseEntity<String> addMember(@Valid Member member, BindingResult result) {
        member.setRegisteredOn(LocalDate.now());

        if (!result.hasErrors() && service.addMember(member)) {
            return ResponseEntity.ok("Successfully Added");
        } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Incomplete or Invalid Data: " + result.toString());
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Member> getMembers() {
        return service.getMembers();
    }

    @ModelAttribute
    public Member setupMember(@RequestParam(name = "memberId", required = false) String id) {
        if (Objects.isNull(id) || id.isEmpty()) {
            return null;
        }

        int memberId = Integer.parseInt(id);

        if (memberId == 0) {
            return null;
        } else {
            return service.getMemberById(memberId);
        }
    }
}
