<%-- 
    Document   : viewPublishers
    Created on : 21-Jan-2020, 6:02:10 PM
    Author     : DigitallSchool <rupeshkumar@digitallschool.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:choose>
            <c:when test="${not empty publishersData}">
                <table border="1">
                    <tr>
                        <th>ID</th><th>Name</th><th>Email</th>
                    </tr>
                    
                    <c:forEach items="${publishersData}" var="pub">
                        <tr>
                            <td>${pub.publisherId}</td>
                            <td>${pub.name}</td>
                            <td>${pub.email}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <h3>No Publishers available.</h3>
            </c:otherwise>
        </c:choose>
    </body>
</html>
