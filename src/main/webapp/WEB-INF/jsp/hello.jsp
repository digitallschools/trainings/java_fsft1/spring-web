<%-- 
    Document   : hello
    Created on : 21-Jan-2020, 4:11:57 PM
    Author     : DigitallSchool <rupeshkumar@digitallschool.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%= java.time.LocalDateTime.now()%><br/><br/>
        
        ${message}
    </body>
</html>
